%% WTE2 (part 1) - 3D tracking of particles inside an evaporating droplet 
%
% In this tutorial you will learn to apply DefocusTracker to measure flows
% in a real application. You will find the 3D position of tracer particles 
% at different time instant and then apply a tracking method to obtain
% velocity and particle trajectories. Finally you will scale the data to
% obtain results in physical units. You can read more detail on the
% experimental setup in 
% <https://doi.org/10.1103/PhysRevE.100.033103 Rossi et al., Phys. Rev. E 
% *100*, 033103 (2019)>.


%% 0 - Instruction
% 
% * Install DefocusTracker (see the README.md file)
% * Complete <Work_through_ex0.html WTE0> and <Work_through_ex1.html WTE1>. 
% * Download the <https://defocustracking.com/Datasets/DT-WTE2-20.zip Datasets-WTE2>
% and unzip it into the folder 'workthrough-examples//Datasets-WTE2/'. 
% * Follow the instructions and run this script cell-by-cell using the command (Ctrl+Enter).


%% 1 - Create and train the model
% Experimental training images are typically taken by 
% scanning along z tracer particles at fixed location (e.g. particle 
% sedimented on the bottom of a microchannel)
%
% In comparison with WTE1 we used here more advanced features with
% smoothing and interpolation. We smoothed the calibration images and
% interpolate them to obtain 100 images from 51. You can try to vary
% these parameters (smoothing typically between 0.1 and 0.2,
% n_inter_images typically no more than 100) to improve the results in
% noisy data.

clc, clear, close all

%  Load the training images:
myfolder = './/Datasets-WTE2/Scan-step2um//';
img_train = dtracker_create('imageset',myfolder);

%  Create the training dataset
N_cal = img_train.n_frames;
dat_train = dtracker_create('dataset',N_cal);
dat_train.fr(:) = 1:N_cal;
dat_train.X(:) = zeros(1,N_cal) + 350; %350 is the X-coord. of the selected particle
dat_train.Y(:) = zeros(1,N_cal) + 588; %855 is the Y-coord. of the selected particle
dat_train.Z(:) = linspace(0,1,N_cal);

%  Inspect the training set
dtracker_show(img_train,dat_train)

%%
%  Create the model with default parameters
model_wte2 = dtracker_create('model');

%  Edit the relevant field in training parameters
model_wte2.training.imwidth = 45;            % width of the particle image
model_wte2.training.imheight = 39;           % height of the particle image
model_wte2.training.median_filter = 3;       % median filter to reduce thermal noise
model_wte2.training.smoothing = 0.2;         % smoothing (optional)
model_wte2.training.n_interp_images = 100;   % interpolation (optional)

%  Train the *model*
model_wte2 = dtracker_train(model_wte2,img_train,dat_train); 

%  Inspect the *model*
dtracker_show(model_wte2)

%% 2 - Validate the model on the training data
% You can obtain a first validation of your model by running a processing
% on the full training data. Since the particles in one frame are all lying
% in the same physical plane, you can determine the true Z position of the
% particles as a function the frame number.

model_wte2.processing.cm_guess = .5;  
model_wte2.processing.cm_final = .9;  
dat_valid = dtracker_process(model_wte2,img_train,1:2:N_cal);
%%
figure
plot((dat_valid.fr-1)/(N_cal-1),dat_valid.Z,'.',[0 1],[0 1])
grid on
xlabel('Z_{true}')
ylabel('Z')

%% 3 - Check the processing parameters on a subset of data
% You can now run the processing on the experimental images. In this
% tutorial experimental images are tracer particles inside an evaporating 
% droplets. The setup and physics of the experiment is described in
% <https://doi.org/10.1103/PhysRevE.100.033103 Rossi et al., Phys. Rev. E 
% *100*, 033103 (2019)>. 
%
% Before running the experiment on the full dataset, it is a good idea to
% try it on few images and adjust the parameters.

%  Load the experimental images
myfolder = './/Datasets-WTE2/Drop-UPW//';
img_exp = dtracker_create('imageset',myfolder);

%  Set the processing parameters and run the evluation on the first 4 frames
model_wte2.processing.cm_guess = .5;  
model_wte2.processing.cm_final = .9; 
dat_exp = dtracker_process(model_wte2, img_exp, 1:3);

%  Inspect the results and change setting if not satisfied.
dtracker_show(dat_exp, img_exp)

%% 4 - Check the tracking parameters on a subset of data
% After we have determined the 3D position of the particles, we want to 
% track their displacement to calculate their velocity and trajectory.
% In DefocusTracker, you can use the function *dtracker_postprocess()* to
% apply a tracking scheme to a *dataset*. As for the processing, you need 
% to create a tracking data stucture first, change the suitable parameters, 
% and then apply it to a *dataset*. 
% The default tracking scheme in DefocusTracker is the nearest neighbor
% approach. This has only 2 parameters:
%
% * tracking_step, that indicates after how many frames we are searching the
% particles to connect. In this case (tracking_step = 1), we connect 
% particles in one frame with particles to the next frame.
% * bounding_box, that indicate the minimum and maximum allowed 
% displacement in X, Y, and Z direction, respectivealy.

tracking_wte2 = dtracker_create('tracking');
tracking_wte2.tracking_step = 1;
tracking_wte2.bounding_box = [-30 30 -30 30 -0.12 0.12]; %  [DXmin DXmax DYmin DYmax DZmin DZmax]
dat_exp_track = dtracker_postprocess(tracking_wte2,dat_exp);

dtracker_show(dat_exp_track, img_exp)


%% 5 - Scale the data to real units
% After image processing, the particle positions X,Y,Z and corresponding 
% displacements DX,DY,DZ are given in normalized units. X and Y are 
% normalized with the size of an image pixel, whereas Z is normalized with 
% the measurement depth. To obtain the value of position and velocity in 
% real units, we need to calculate the scaling factors:
%
% * x = X*(camera pixel size)/(magnification in X-direction) 
% * y = Y*(camera pixel size)/(magnification in Y-direction) 
% * z = Z*(number of frames in the training set - 1)*step length*refractive index of the fluid
% * vx = DX*(camera pixel size)/(magnification in X-direction)/Dt
% * vy = DY*(camera pixel size)/(magnification in X-direction)/Dt
% * vz = DZ*(number of frames in the training set - 1)*step length*
% refractive index of the fluid/Dt
%
% Dt is the time between two frames. In DefocusTracker you can create a 
% scaling structure and then apply to the *dataset* using the function 
% *dtracker_postprocess()*.
%

%  Create a default scaling and edit the relevant fields
scaling_wte2 = dtracker_create('scaling');
scaling_wte2.X_to_x = 1.2659; 
scaling_wte2.Y_to_y = 1.1965;
scaling_wte2.Z_to_z = -(N_cal-1)*2*1.33;
scaling_wte2.dt = 10;
scaling_wte2.unit = 'um';

%  Apply the scaling
dat_exp_track_scaled = dtracker_postprocess('scale',dat_exp_track,scaling_wte2);

%% 
% Note the different variable names in a *dataset* are different if it is
% scaled (uppercase) or unscaled (lowercase). 

disp(head(dat_exp_track,5))

%%
disp(head(dat_exp_track_scaled,5))

%%
% In DefocusTracking, you have a interactive windows to calculated the 
% scaling factors from a picture of a calibration grid of known dimension. 
% In this example we used a calibration grid with a 100 um pitch. Open the 
% windows and try the following steps:
%
% * Run command: dtracker_create('scaling')
% * Load image '.Datasets-WTE2\Grid-100um\B00001.tif'
% * Push 'select cross'
% * Select a square around a cross, double click on the rectangle to
%  confirm
% * Insert the Pitch value (100 and 100)
% * Insert the unit ('um')
% * Push 'Calculate grid'
% * Insert Z Depth: (51-1)*2*1.33
% * Inser Dt: 10 
% * Insert scaling name: scaling_wte2
% * Push 'Save'

dtracker_create('scaling')

%% 6 - Run the full processing on all datasets
% Once you have checked that all your settings are right on few images, you
% can run the full processing on the entire *imageset*. In DefocusTracker
% you can append the tracking and scaling to your model, by creating the
% field 'tracking' and 'scaling'. Or if you prefer you can do that in
% separate steps as done so far. You can always apply a new scaling and a
% new tracking to a *dataset*. 

%  Add the tracking and scaling to the model
model_wte2.tracking = tracking_wte2;
model_wte2.scaling = scaling_wte2;

%  Run the processing on all data frames
dat_exp = dtracker_process(model_wte2,img_exp);

%  Plot the tracks
figure
dtracker_show(dat_exp,'plot_3d_tracks')

%% 7 - Save the data
% Congratulation, you get your first experimental results with 
% DefocusTracer! Do not forget to save all your relevant data! In part 2 of
% WTE2 you will learn to correct bias and make nicer plots of your data.

myfolder = './/Datasets-WTE2';
save(myfolder,'dat_train','dat_valid','dat_exp','model_wte2')


%% Bibliography
%
% <https://doi.org/10.1103/PhysRevE.100.033103 Interfacial flows in sessile
% evaporating droplets of mineral water>, M. Rossi, A. Marin, and C.J. Kähler,
% Phys. Rev. E *100*, 033103  (2019).
%
% <https://doi.org/10.1007/s00348-020-2937-5 General Defocusing Particle
% Tracking: Fundamentals and uncertainty assessment>, R. Barnkob and M.
% Rossi, Exp. Fluids *61*, 110 (2020).
%
% <https://doi.org/10.1088/1361-6501/abad71 A fast and robust algorithm for
% general defocusing particle tracking>, M. Rossi and R. Barnkob, Meas. Sci.
% Technol. *32*, 014001 (2020).
%