%% WTE0 - Basics: Inputs and outputs
%
% In this tutorial you will familiarize with the standard INPUT/OUTPUT used
% in <https://defocustracking.com/defocustracker/ DefocusTracker>,
% which are *imageset* and *dataset*, respectively. You will also start to 
% use the basic functions *dtracker_create()* and *dtracker_show()*.

%% 0 - Instruction
% 
% * Install DefocusTracker (see the README.md file)
% * Follow the instructions and run this script cell-by-cell using the command (Ctrl+Enter).

%% 1 - INPUT: Create an imageset
%
% DefocusTracker takes as INPUT a set of TIFF images located into one
% folder. In DefocusTraker you create an *imageset* with the function 
% *dtracker_create()*. You can create an *imageset* interactively:   
%
%   myimageset = dtracker_create('imageset');
%
% Or you can decleare the folder where the images are:

clc, clear, close all
myfolder = './/Datasets-WTE0//Images//';
img_myflow = dtracker_create('imageset',myfolder);

%% 2 - INPUT: Inspect an imageset
% In DefocusTracker you can interactively look at an *imageset* using
% the function *dtracker_show()*. 

dtracker_show(img_myflow)

%%
% You can output the n'th frame of an *imageset*in form of a MATLAB
% array using:
%
frame_1 = dtracker_show(img_myflow, 1);


%% 3 - OUTPUT: Create a dataset
% The OUTPUT of a DefocusTracker evaluation is a list of variables 
% organized in a <https://www.mathworks.com/help/matlab/ref/table.html MATLAB table>.
% A dataset is created as an output of the function *dtracker_process()* as
% shown in the following Work-Through Example 1 (WTE1). 

load('.//Datasets-WTE0//datasets_wte0');
disp(head(dat_myflow)) % see first few rows 

%%
% By default, a DefocusTracker dataset contains 9 fields:
%
% #   fr: frame number
% #   X: horizontal coordinates in pixel as unit
% #   Y: vertical coordinates in pixel as unit
% #   Z: depth coordinate normalized with meas. depth (values btw 0 and 1)
% #   DX: horizontal displacement in pixel as unit
% #   DY: vertical displacement in pixel as unit
% #   DZ: depth displacement normalized with measurement depth
% #   id: identity number of one track
% #   cm: value rating how trustable a measurement is (values btw 0 and 1)
%
% You can create a ZEROS dataset with n rows using:
%
dat_zeros = dtracker_create('dataset', 3);
disp(dat_zeros)

%% 4 - OUTPUT: Inspect a dataset
% In DefocusTracker you can interactively look at a dataset using
% the function *dtracker_show()*. 

dtracker_show(dat_myflow)

%% 5 - OUTPUT: Work with a dataset
% Here few example about how to apply flags and merge *datasets*. For more 
% info on how to work with tables see the 
% <https://www.mathworks.com/help/matlab/ref/table.html MATLAB documentation>.

% Take all data in Frame 1
flag = dat_myflow.fr==1;
dat_myflow_fr1 = dat_myflow(flag,:);

% Take all data in Frame 2
flag = dat_myflow.fr==2;
dat_myflow_fr2 = dat_myflow(flag,:);

% Merge dat_myflow_fr1 and dat_myflow_fr2
dat_myflow_fr12 = [dat_myflow_fr1; dat_myflow_fr2];

%% 6 - INPUT & OUTPUT: Display an imageset + dataset
% The function *dtracker_show()* can be used also to display together 
% one *imageset* and the respective *dataset*. 

dtracker_show(img_myflow,dat_myflow)

