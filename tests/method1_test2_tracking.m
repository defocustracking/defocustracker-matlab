%% Test 2: Test method_1 on WTE2 data (duration about 1 minute): 
%  Initialize (this path will work fine if Datesets folder are copied into
%  the 'workthrough-example' folder.

%% Test 2.0: Inizialize 
clc, clear, close all
disp('--------- Test2 STARTED ---------')
path_wte2 = '..\workthrough-examples\Datasets-WTE2';

%% Test 2.1: Create the model and the training set
model2 = dtracker_create('model');
img_train2 = dtracker_create('imageset',[path_wte2,'//Scan-step2um']);
N_cal = img_train2.n_frames;
dat_train2 = dtracker_create('dataset',N_cal);
dat_train2.fr(:) = 1:N_cal;
dat_train2.X(:) = zeros(1,N_cal) + 350;
dat_train2.Y(:) = zeros(1,N_cal) + 588;
dat_train2.Z(:) = linspace(0,1,N_cal);

%% Test 2.2: Train the model
model2.training.imwidth = 45;            % width of the particle image
model2.training.imheight = 39;           % height of the particle image
model2.training.median_filter = 3;       % median filter to reduce thermal noise
model2.training.smoothing = 0.2;         % smoothing (optional)
model2.training.n_interp_images = 100;   % interpolation (optional)
model2 = dtracker_train(model2,img_train2,dat_train2); 

%% Test 2.3: Run the processing

tracking2 = dtracker_create('tracking');
tracking2.tracking_step = 1;
tracking2.bounding_box = [-30 30 -30 30 -0.12 0.12]; 

scaling2 = dtracker_create('scaling');
scaling2.X_to_x = 1.2659; 
scaling2.Y_to_y = 1.1965;
scaling2.Z_to_z = -(N_cal-1)*2*1.33;
scaling_wte2.dt = 10;
scaling_wte2.unit = 'um';

img_exp2 = dtracker_create('imageset',[path_wte2,'//Drop-UPW']);
N_frames = img_exp2.n_frames;

model2.processing.cm_guess = .4;                
model2.processing.cm_final = .5;                
model2.tracking = tracking2;           
model2.scaling = scaling2;

n_frames_test = 1:10;
[dat_exp2, time2] = dtracker_process(model2, img_exp2, n_frames_test);

%% Test 2.4: Postprocess  (min_n_d)
min_length_of_track = 7;
dat_exp2_pp1 = dtracker_postprocess('min_n_id',dat_exp2,min_length_of_track);
n_tracks = length(unique(dat_exp2_pp1.id));

%% Test 2.5: Summary

dat_exp2_pp1 = dtracker_postprocess('unscale',dat_exp2_pp1);
disp('          ')
disp('--------- Test2 summary ---------')
disp('parameter = value (reference value*)')
disp('*reference values obtained with defocustracker v.1.0.2 ')
disp('on a laptop with i7-8665U CPU @ 1.90GHz, 16 GB RAM')
disp('--------------------------------')
disp(['Mean DX [pixel] = ',num2str(mean(dat_exp2_pp1.DX)),' (3.16)'])
disp(['Mean DY [pixel] = ',num2str(mean(dat_exp2_pp1.DY)),' (-5.34)'])
disp(['Mean DZ [-] = ',num2str(mean(dat_exp2_pp1.DZ)),' (0.02)'])
disp(['Number of tracks = ',num2str(n_tracks),' (129)'])
disp(['Processing time [sec] = ',num2str(time2,'%0.1f'),'(46.3)'])
